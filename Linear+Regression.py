
# coding: utf-8

# In[122]:

import matplotlib.pyplot as plt
import numpy


# In[126]:

class linearRegression:
    def __init__(self, hypothesis, theta, x, y, alpha):
        self.hypothesis = hypothesis
        self.alpha = alpha
        self.theta = theta
        self.x = x
        self.y = y
        pass
    def cost(self):
        f = 2*len(self.x)
        s = 1/f
        total = 0.0
        for i in range(len(self.x)):
            total += pow(self.hypothesis(self.x[i], self.theta) - self.y[i],2)
            pass
        cost = s*total
        return cost
        pass
    def train(self):
        thetaSum = []
        for i in range(len(self.theta)):
            thSum = 0
            for j in range(len(self.x)):
                if i < 1:
                    thSum += (self.hypothesis(self.x[j], self.theta) - self.y[j]) * 1.0 #Bias
                else:
                    thSum += (self.hypothesis(self.x[j], self.theta) - self.y[j]) * self.x[j]
                pass
            thetaSum.append(thSum)
            pass
        for i in range(len(self.theta)):
            self.theta[i] = self.theta[i] - (self.alpha * (1/len(self.x)) * thetaSum[i])
            pass


# In[129]:

#Linear
hypothesis = lambda x, theta: theta[0] + theta[1]*x
lin = linearRegression(hypothesis,[1,0],[0,1,2,3,4],[4,-1,-8,-8,-22],0.01)
print('Init cost: ' + str(lin.cost()))
for i in range(1000):
    lin.train()
    pass
x = numpy.linspace(lin.x[0],lin.x[-1],300)
y = hypothesis(x,lin.theta)
plt.plot(x,y)
print('Final cost: ' + str(lin.cost()))
plt.plot(lin.x, lin.y, 'ro')
plt.show()


# In[140]:

#Polynomial
hypothesis = lambda x, theta: theta[0] + theta[1]*x + theta[2]*pow(x,2)

xP = [0,1,2,3,4,5]
yP = [100,10,100,300,500,1000]

lin = linearRegression(hypothesis,[1,0,0],xP,yP,0.01)
print('Init cost: ' + str(lin.cost()))
for i in range(1000):
    lin.train()
    pass

x = numpy.linspace(lin.x[0],lin.x[-1],300)
y = hypothesis(x,lin.theta)
plt.plot(x,y)
print('Final cost: ' + str(lin.cost()))
plt.plot(lin.x, lin.y, 'ro')
plt.show()


# In[ ]:




# In[ ]:



